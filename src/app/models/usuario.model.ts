export class Usuario {
  constructor(
    public usu_nombres?: string,
    public usu_apellidos?: string,
    public usu_cedula?: string,
    public usu_email?: string,
    public usu_password?: string,
    public usu_rol?: string,
    public id?: string
  ) {}
}
