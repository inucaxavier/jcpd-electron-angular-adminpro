export class DatosDenunciante {
  constructor(
    public caso_id?: number,
    public dden_nombre?: string,
    public dden_apellido?: string,
    public dden_ci?: string,
    public tper_id?: number,
    public dden_fecha_nacimento?: string,
    public dden_edad?: string,
    public dden_relacion_afectado?: string,
    public gene_id?: number,
    public iden_id?: number,
    public prov_id?: number,
    public can_id?: number,
    public sec_id?: number,
    public parr_id?: number,
    public dden_direccion?: string,
    public dden_telefono?: string,
    public dden_email?: string,
    public dden_observaciones?: string
  ) {}
}
