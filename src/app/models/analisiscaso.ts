export class AnalisisCaso {
  constructor(
    public anc_id?: number,
    public caso_id?: number,
    public tica_id?: number,
    public anc_observacion?: string
  ) {}
}
