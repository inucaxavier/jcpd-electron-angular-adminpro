export class Audiencia {
  constructor(
    public caso_id?: number,
    public rau_fecha_conciliacion?: Date,
    public rau_hora_audiencia?: string,
    public rau_observacion?: string,
    public rau_resumen?: string,
    public rau_conciliacion?: string
  ) {}
}
