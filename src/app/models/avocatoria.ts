export class Avocatoria {
  constructor(
    public anc_id?: number,
    public avo_fecha_conciliacion?: Date,
    public avo_observaciones?: string,
    public avo_hora_audiencia?: string
  ) {}
}
