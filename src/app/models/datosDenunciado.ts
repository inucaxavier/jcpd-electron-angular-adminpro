export class DatosDenunciado {
  constructor(
    public caso_id?: number,
    public dag_nombre?: string,
    public dag_apellido?: string,
    public dag_ci?: string,
    public dag_relacion_afectado?: string,
    public tper_id?: number,
    public gene_id?: number,
    public iden_id?: number,
    public prov_id?: number,
    public can_id?: number,
    public sec_id?: number,
    public parr_id?: number,
    public dag_direccion?: string,
    public dag_telefono?: string,
    public dag_email?: string,
    public dag_observacion?: string
  ) {}
}