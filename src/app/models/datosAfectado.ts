export class DatosAfectado {
  constructor(
    public caso_id?: number,
    public daf_nombre?: string,
    public daf_apellido?: string,
    public daf_ci?: string,
    public daf_fecha_nacimiento?: Date,
    public daf_edad?: number,
    public iden_id?: number,
    public gene_id?: number,
    public dis_id?: number,
    public prov_id?: number,
    public can_id?: number,
    public parr_id?: number,
    public daf_direccion?: string,
    public daf_telefono?: string,
    public daf_email?: string,
    public daf_obsevacion?: string
  ) {}
}
