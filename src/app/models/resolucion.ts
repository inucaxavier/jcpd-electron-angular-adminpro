export class Resolucion {
  constructor(
    public res_id?: number,
    public rau_id?: number,
    public tme_id?: number,
    public caso_id?: number,
    public res_resumen_resolucion?: string,
    public res_fecha_inicio_media?: Date,
    public res_fecha_conclucion_media?: Date,
    public res_responsable?: string
  ) {}
}
