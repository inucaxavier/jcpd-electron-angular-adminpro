export class Caso {
  constructor(
    public caso_id?: number,
    public caso_numcaso?: string,
    public proco_id?: number,
    public tda_id?: number,
    public caso_motivo?: string,
    public caso_observaciones?: string,
    public caso_fechareg?: Date,
    public caso_fechaingreso?: Date,
    public dav_id?: number,
    public tdenun_id?: number,
    public viewCount?: number,
    public published?: boolean
  ) {}
}
