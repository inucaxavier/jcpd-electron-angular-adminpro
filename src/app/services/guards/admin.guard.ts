import { UsuarioService } from 'src/app/services/service.index';
import { Injectable } from '@angular/core';

import { CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor(public _usuarioService: UsuarioService) {}

  canActivate() {
    if (this._usuarioService.usuario.usu_rol === 'ADMIN_ROLE') {
      return true;
    } else {
      console.log('bloqueado por el admin guard');
      this._usuarioService.logout();
      return false;
    }
  }
}
