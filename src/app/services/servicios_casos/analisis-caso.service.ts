import { AnalisisCaso } from './../../models/analisiscaso';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Avocatoria } from '../../models/avocatoria';
import swal from 'sweetalert';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { URL_SERVICIOS } from './../../config/config';

@Injectable({
  providedIn: 'root'
})
export class AnalisisCasoService {
  constructor(public http: HttpClient) {}
  analisisCaso = AnalisisCaso;

  crearAnalisis(analisis: AnalisisCaso) {
    const url = URL_SERVICIOS + '/analisiscaso';

    return this.http.post(url, analisis).pipe(
      map((resp: any) => {
        swal('Analisis Creado', '', 'success');
        return resp.analisis;
      }),
      catchError(err => {
        swal('Error Crear Analasis', err.error, 'error');
        return throwError(err);
      })
    );
  }
}
