import { DatosDenunciante } from './../../models/datosDenunciante';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_SERVICIOS } from 'src/app/config/config';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  constructor(private http: HttpClient) {}
  private _refrescarNeeds = new Subject<void>();

  get refrescaNeeds() {
    return this._refrescarNeeds;
  }

  cargarParro1(): Observable<DatosDenunciante[]> {
    let url = URL_SERVICIOS + '/api/parrosum';
    return this.http.get<DatosDenunciante[]>(url);
  }
}
