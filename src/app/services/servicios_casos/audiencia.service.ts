import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Audiencia } from '../../models/audiencia';
import swal from 'sweetalert';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { URL_SERVICIOS } from './../../config/config';
@Injectable({
  providedIn: 'root'
})
export class AudienciaService {
  constructor(public http: HttpClient, public router: Router) {}
  audiencia: Audiencia;

  crearAudiencia(audiencia: Audiencia) {
    let url = URL_SERVICIOS + '/api/avocatoria';
    return this.http.post(url, audiencia).pipe(
      map((resp: any) => {
        swal('Audiencia Creada', '', 'success');
        return resp.audiencia;
      }),
      catchError(err => {
        swal('Error Crear Audiencia', err.error, 'error');
        return throwError(err);
      })
    );
  }
  cargarAudiencia() {
    let a = URL_SERVICIOS + '/api/audiencia';
    console.log(a);
    return this.http.get(a);
  }
}
