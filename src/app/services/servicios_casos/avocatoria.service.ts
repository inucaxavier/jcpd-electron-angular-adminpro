import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Avocatoria } from '../../models/avocatoria';
import swal from 'sweetalert';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { URL_SERVICIOS } from './../../config/config';

@Injectable({
  providedIn: 'root'
})
export class AvocatoriaService {
  avocatoria: Avocatoria;
  constructor(public http: HttpClient, public router: Router) {}

  crearAvocatoria(avocatoria: Avocatoria) {
    let url = URL_SERVICIOS + '/api/avocatoria';
    return this.http.post(url, avocatoria).pipe(
      map((resp: any) => {
        swal('Avocatoria Creada', '', 'success');
        return resp.avocatoria;
      }),
      catchError(err => {
        swal('Error Crear Avocatoria', err.error, 'error');
        return throwError(err);
      })
    );
  }

  cargarAvocatoria() {
    let url = URL_SERVICIOS + '/api/avocatoria';
    console.log(url);
    return this.http.get(url);
  }
}
