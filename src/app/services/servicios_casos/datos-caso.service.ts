import { DatosAfectado } from '../../models/datosAfectado';
import { DatosDenunciado } from './../../models/datosDenunciado';
import { DatosDenunciante } from 'src/app/models/datosDenunciante';
import { Router } from '@angular/router';
import swal from 'sweetalert';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { URL_SERVICIOS } from './../../config/config';

@Injectable({
  providedIn: 'root'
})
export class DatosCasoService {
  API_URI = 'http://localhost:8080/api';

  constructor(private http: HttpClient, public router: Router) {}

  getDatosAfectados() {
    return this.http.get(`${this.API_URI}/datosafectado`);
  }
  getDatosAfectado(id: string) {
    return this.http.get(`${this.API_URI}/datosafectado/${id} `);
  }
  deleteDatosAfectado(id: string) {
    return this.http.delete(`${this.API_URI}/datosafectado/${id}`);
  }

  /* saveDatosAfectado(datosafectado: DatosAfectado) {
    return this.http.post(`${this.API_URI}/datosafectado`, datosafectado);
  } */

  crearAfectado(datosafectado: DatosAfectado) {
    let url = URL_SERVICIOS + '/api/datosafectado';
    return this.http.post(url, datosafectado).pipe(
      map((resp: any) => {
        swal(
          'Afectado Creado',
          datosafectado.daf_nombre + '' + datosafectado.daf_apellido,
          'success'
        );
        return resp.datosafectado;
      }),
      catchError(err => {
        swal('Error Crear Afectado', err.error, 'error');
        return throwError(err);
      })
    );
  }
  crearDenunciado(datosdenunciado: DatosDenunciado) {
    let url = URL_SERVICIOS + '/api/datosdenunciado';
    return this.http.post(url, datosdenunciado).pipe(
      map((resp: any) => {
        swal('Denunciado Creado', datosdenunciado.dag_nombre, 'success');
        return resp.datosdenunciado;
      }),
      catchError(err => {
        swal('Error Crear Denunciado', err.error, 'error');
        return throwError(err);
      })
    );
  }
  crearDenunciante(datosdenunciante: DatosDenunciante) {
    let url = URL_SERVICIOS + '/api/datosdenunciante';
    return this.http.post(url, datosdenunciante).pipe(
      map((resp: any) => {
        swal('Denunciante Creado', datosdenunciante.dden_nombre, 'success');
        return resp.datosdenunciante;
      }),
      catchError(err => {
        swal('Error Crear Denunciante', err.error, 'error');
        return throwError(err);
      })
    );
  }

  updateDatosAfectado(id: string, updatedDatosAfectado: DatosAfectado) {
    return this.http.put(
      `${this.API_URI}/datosafectado/${id}`,
      updatedDatosAfectado
    );
  }

  // //////-------------------------------------------------///
  // Contribuyentes de otras tablas

  getCaso() {
    return this.http.get(`${this.API_URI}/caso`);
  }

  getIdentidad() {
    return this.http.get(`${this.API_URI}/identidad`);
  }
  getGenero() {
    return this.http.get(`${this.API_URI}/genero`);
  }
  getDiscapacidad() {
    return this.http.get(`${this.API_URI}/discapacidad`);
  }
  getParroquia() {
    return this.http.get(`${this.API_URI}/parroquias`);
  }
  getTpersona() {
    return this.http.get(`${this.API_URI}/tpersona`);
  }
}
