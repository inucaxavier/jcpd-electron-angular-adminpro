import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Caso } from 'src/app/models/caso.model';
import { URL_SERVICIOS } from './../../config/config';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class JcpdService {
  constructor(private http: HttpClient) {}

  private _refrescarNeeds = new Subject<void>();

  getCaso(caso_numcaso: string): Observable<Caso> {
    let url = URL_SERVICIOS + '/api/casos/' + caso_numcaso;

    return this.http.get<Caso>(url);
  }

  get refrescaNeeds() {
    return this._refrescarNeeds;
  }

  cargarCasos(): Observable<Caso[]> {
    let url = URL_SERVICIOS + '/api/casos';
    return this.http.get<Caso[]>(url);
  }

  togglePublishState(caso: Caso): Observable<Caso> {
    return this.http
      .post<Caso>(environment.apiUrl + '/dashboard/casos/publish', caso)
      .pipe(
        tap(() => {
          this._refrescarNeeds.next();
        })
      );
  }

  cargarHistoricos(): Observable<Caso[]> {
    let url = URL_SERVICIOS + '/api/historiaCasos';
    return this.http.get<Caso[]>(url);
  }
}
