import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Caso } from '../../models/caso.model';
import { Observable } from 'rxjs';
import swal from 'sweetalert';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { URL_SERVICIOS } from './../../config/config';

@Injectable({
  providedIn: 'root'
})
export class CasosService {
  casosli: Caso;

  // Coneccion al servidor de NODE, DONDE TENEMOS LA BASE DE DATOS
  API_URI = 'http://localhost:8080/api';

  constructor(private http: HttpClient, public router: Router) {}

  deleteCaso(id: string) {
    return this.http.delete(`${this.API_URI}/casos/${id}`);
  }

  saveCaso(caso: Caso) {
    let url = URL_SERVICIOS + '/api/casos';

    return this.http.post(url, caso).pipe(
      map((resp: any) => {
        swal('Caso Creado', caso.caso_numcaso, 'success');
        return resp.caso;
      }),
      catchError(err => {
        swal('Error Crear Caso', err.error, 'error');
        return throwError(err);
      })
    );
  }

  cargarCasos() {
    let url = URL_SERVICIOS + '/api/casos';
    return this.http.get(url);
  }

  /* cargarCasos(): Observable<Caso[]> {
    return this.http.get<Caso[]>('http://localhost:8080/api/casos');
  } */

  /* getCasos(): Observable<Caso[]> {
    return this.http.get<Caso[]>(`${this.API_URI}/casos`);
  } */
  getCaso(caso_numcaso: string): Observable<Caso[]> {
    return this.http.get<Caso[]>(`${this.API_URI}/casos/` + caso_numcaso);
  }

  updateCaso(id: string, updatedCaso: Caso): Observable<Caso> {
    return this.http.put(`${this.API_URI}/casos/${id}`, updatedCaso);
  }

  // //////-------------------------------------------------///
  // Contribuyentes de otras tablas

  getProco() {
    return this.http.get(`${this.API_URI}/proco`);
  }
  getTipoDenuncia() {
    return this.http.get(`${this.API_URI}/tipoDenuncia`);
  }
  getDerechoAmeViolentado() {
    return this.http.get(`${this.API_URI}/tipoDerechoAmenazas`);
  }
  getTiposDerechosAmenazas() {
    return this.http.get(`${this.API_URI}/amenazadoViolentado`);
  }
}
