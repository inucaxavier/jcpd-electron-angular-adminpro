import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from './../../config/config';
import { Resolucion } from './../../models/resolucion';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ResolucionService {
  constructor(private http: HttpClient) {}

  traerResolucion() {
    let url = URL_SERVICIOS + '/api/resolucion';
    console.log(url);
    return this.http.get(url);
  }
}
