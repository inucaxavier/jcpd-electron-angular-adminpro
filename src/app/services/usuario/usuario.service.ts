import { URL_SERVICIOS } from './../../config/config';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Usuario } from './../../models/usuario.model';
import { Injectable } from '@angular/core';
import swal from 'sweetalert';
import { throwError } from 'rxjs';

@Injectable()
export class UsuarioService {
  usuario: Usuario;
  token: string;
  menu: any[] = [];
  constructor(public http: HttpClient, public router: Router) {
    this.cargarStorage();
  }

  estaLogueado() {
    return this.token.length > 5 ? true : false;
  }

  cargarStorage() {
    if (localStorage.getItem('token')) {
      this.token = localStorage.getItem('token');
      this.usuario = JSON.parse(localStorage.getItem('usuario'));
      this.menu = JSON.parse(localStorage.getItem('menu'));
    } else {
      this.token = '';
      this.usuario = null;
      this.menu = [];
    }
  }

  guardarStorage(id: string, token: string, usuario: Usuario, menu: any) {
    localStorage.setItem('id', id);
    localStorage.setItem('token', token);
    localStorage.setItem('usuario', JSON.stringify(usuario));
    localStorage.setItem('menu', JSON.stringify(menu));

    this.usuario = usuario;
    this.token = token;
    this.menu = menu;
  }

  logout() {
    this.usuario = null;
    this.token = '';
    this.menu = [];

    localStorage.removeItem('token');
    localStorage.removeItem('usuario');
    localStorage.removeItem('menu');
    this.router.navigate(['/login']);
  }

  login(usuario: Usuario, recordar: boolean = false) {
    if (recordar) {
      localStorage.setItem('usu_email', usuario.usu_email);
    } else {
      localStorage.removeItem('usu_email');
    }
    let url = URL_SERVICIOS + '/login';
    return this.http.post(url, usuario).pipe(
      map((resp: any) => {
        this.guardarStorage(resp.id, resp.token, resp.usuario, resp.menu);
        return true;
      }),
      catchError(err => {
        swal('Error en el Login', err.error.reason, 'error');

        return throwError(err);
      })
    );
  }

  crearUsuario(usuario: Usuario) {
    let url = URL_SERVICIOS + '/usuario';
    return this.http.post(url, usuario).pipe(
      map((resp: any) => {
        swal('Usuario Creado', usuario.usu_email, 'success');
        return resp.usuario;
      }),
      catchError(err => {
        swal('Error Crear Usuario', err.error, 'error');
        return throwError(err);
      })
    );
  }

  cargarUsuarios() {
    let url = URL_SERVICIOS + '/usuarios';
    return this.http.get(url);
  }

  borrarUsuario(id: string) {
    let url = URL_SERVICIOS + '/usuario/' + id;
    url += '?token=' + this.token;

    return this.http.delete(url).pipe(
      map((resp: any) => {
        swal(
          'Usuario Borrado',
          'El Usuario ha sido eliminado correctamente',
          'success'
        );
        return true;
      })
    );
  }

  actualizarUsuario(usuario: Usuario) {
    let url = URL_SERVICIOS + '/usuario/' + usuario.id;
    url += '?token=' + this.token;

    return this.http.put(url, usuario).pipe(
      map((resp: any) => {
        let usuarioDB: Usuario = resp.usuario;
        this.guardarStorage(usuarioDB.id, this.token, usuarioDB, this.menu);
        swal('Usuario actualizado', usuario.usu_nombres, 'success');
        return true;
      })
    );
  }

  actualizarUsuarioRol(usuario: Usuario) {
    let url = URL_SERVICIOS + '/usuario/' + usuario.id;
    console.log(url);

    url += '?token=' + this.token;

    return this.http.put(url, usuario).pipe(
      map((resp: any) => {
        console.log('hols' + resp);
        if (usuario.id === this.usuario.id) {
          let usuarioDB: Usuario = resp.usuario;
          this.guardarStorage(usuarioDB.id, this.token, usuarioDB, this.menu);
        }
        swal(
          'Rol de Usuario Actualizado',
          usuario.usu_nombres +
            ' ' +
            usuario.usu_apellidos +
            ' ROL: ' +
            usuario.usu_rol,
          'success'
        );
        return true;
      })
    );
  }

  renuevaToken() {
    let url = URL_SERVICIOS + '/renuevaToken';
    url += '?token=' + this.token;
    return this.http.get(url).pipe(
      map((resp: any) => {
        this.token = resp.token;
        localStorage.setItem('token', this.token);

        console.log('token renovado');

        return true;
      }),
      catchError(err => {
        this.router.navigate(['/login']);
        swal(
          'No se pudo renovar token',
          'No fue posible realizar la renovacion',
          'error'
        );
        return throwError(err);
      })
    );
  }
}
