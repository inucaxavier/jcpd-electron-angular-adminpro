import { Injectable } from '@angular/core';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  menu: any[] = [];
  /* menu: any = [
    {
      titulo: 'Casos',
      icono: 'mdi mdi-gauge',
      submenu: [
        { titulo: 'Lista de Casos', url: '/casos-list' },
        { titulo: 'Registrar Caso', url: '/casos-form' }
      ]
    },
    {
      titulo: 'Avocatorias',
      icono: 'mdi mdi-gauge',
      submenu: [
        { titulo: 'Dashboard', url: '/dashboard' },
        { titulo: 'ProgressBar', url: '/progress' },
        { titulo: 'Usuarios', url: '/usuarios' }
      ]
    }
  ]; */

  constructor(public _usuarioService: UsuarioService) {}

  cargarMenu() {
    if (!this.menu) {
      return;
    }
    this.menu = this._usuarioService.menu;
  }
}
