import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'usuariosFilter'
})
export class UsuariosFilterPipe implements PipeTransform {
  transform(value: any, arg: any): any {
    if (arg === '' || arg.length < 3) return value;
    const resultCaso = [];
    for (const post of value) {
      if (post.usu_nombres.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
        /*      console.log('Si'); */
        resultCaso.push(post);
      } else if (post.usu_email.indexOf(arg) > -1) {
        resultCaso.push(post);
      }
    }
    return resultCaso;
  }
}
