import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroCaso'
})
export class FiltroCasoPipe implements PipeTransform {
  transform(value: any, arg: any): any {
    const resultCasoReg = [];
    for (const caso of value) {
      if (caso.caso_fechareg.indexOf(arg) > -1) {
        resultCasoReg.push(caso);
      }
    }
    return resultCasoReg;
  }
}
