import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(value: any, arg: any): any {
    const resultCaso = [];
    for (const caso of value) {
      if (caso.caso_numcaso.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
        resultCaso.push(caso);
      }
    }
    return resultCaso;
  }
}
