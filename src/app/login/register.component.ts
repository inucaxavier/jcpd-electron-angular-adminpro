import { UsuarioService } from './../services/service.index';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Usuario } from '../models/usuario.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./login.component.css']
})
export class RegisterComponent implements OnInit {
  forma: FormGroup;
  emailV = '[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}';
  public user: any = {};
  verificador = false;
  caracterMin = 4;
  caracterMax = 25;
  email: string;
  constructor(public router: Router, public usuarioService: UsuarioService) {}

  sonIguales(campo1: string, campo2: string) {
    return (group: FormGroup) => {
      let pass1 = group.controls[campo1].value;
      let pass2 = group.controls[campo2].value;

      if (pass1 === pass2) {
        return null;
      }

      return {
        sonIguales: true
      };
    };
  }
  espaciosEnBlanco(p1: string) {
    var espacios = false;
    var cont = 0;

    while (!espacios && cont < p1.length) {
      if (p1.charAt(cont) == ' ') espacios = true;
      cont++;
    }

    if (espacios) {
      alert('La contraseña no puede contener espacios en blanco');
      return false;
    }
  }

  ngOnInit() {
    this.forma = new FormGroup(
      {
        usu_nombres: new FormControl(null, [
          Validators.minLength(this.caracterMin),
          Validators.maxLength(this.caracterMax),
          Validators.required
        ]),
        usu_apellidos: new FormControl(null, [
          Validators.minLength(this.caracterMin),
          Validators.maxLength(this.caracterMax),
          Validators.required
        ]),
        usu_cedula: new FormControl(null, [
          Validators.minLength(this.caracterMin),
          Validators.maxLength(this.caracterMax),
          Validators.required
        ]),
        usu_email: new FormControl(null, [
          Validators.required,
          Validators.pattern(this.emailV)
        ]),
        usu_password: new FormControl(null, Validators.required),
        password2: new FormControl(null, Validators.required),
        condiciones: new FormControl(false)
      },
      { validators: this.sonIguales('usu_password', 'password2') }
    );

    /* this.forma.setValue({
      usu_nombres: 'Test',
      usu_apellidos: 'Testing',
      usu_cedula: '100311198',
      usu_email: 'test@test.com',
      usu_password: '123456',
      password2: '123456',
      condiciones: true
    }); */
  }

  registrarUsuario() {
    if (this.forma.invalid) {
      return;
    }
    /* 
    if (!this.forma.value.condiciones) {
      swal('Importante', 'Debe de aceptar las condiciones', 'warning');
      return;
    } */

    let usuario = new Usuario(
      this.forma.value.usu_nombres,
      this.forma.value.usu_apellidos,
      this.forma.value.usu_cedula,
      this.forma.value.usu_email,
      this.forma.value.usu_password
    );

    this.usuarioService
      .crearUsuario(usuario)
      .subscribe(resp => this.router.navigate(['/usuarios']));
  }

  validadorDeCedula(cedula: String) {
    let cedulaCorrecta = false;
    /*     console.log(cedula.length);
 */
    if (cedula.length == 10) {
      // ConstantesApp.LongitudCedula
      let tercerDigito = parseInt(cedula.substring(2, 3));
      if (tercerDigito < 6) {
        // Coeficientes de validación cédula
        // El decimo digito se lo considera dígito verificador
        let coefValCedula = [2, 1, 2, 1, 2, 1, 2, 1, 2];
        /*         console.log(coefValCedula);
 */
        let verificador = parseInt(cedula.substring(9, 10));
        let suma: number = 0;
        let digito: number = 0;
        for (let i = 0; i < cedula.length - 1; i++) {
          digito = parseInt(cedula.substring(i, i + 1)) * coefValCedula[i];

          suma += parseInt(digito % 10 + '') + parseInt(digito / 10 + '');
          /*           console.log(suma + ' suma' + coefValCedula[i]);
 */
        }
        suma = Math.round(suma);
        /* console.log(verificador);
        console.log(suma);
        console.log(digito); */

        if (
          Math.round(suma % 10) == 0 &&
          Math.round(suma % 10) == verificador
        ) {
          cedulaCorrecta = true;
        } else if (10 - Math.round(suma % 10) == verificador) {
          cedulaCorrecta = true;
        } else {
          cedulaCorrecta = false;
        }
      } else {
        cedulaCorrecta = false;
      }
    } else {
      cedulaCorrecta = false;
    }
    if (!cedulaCorrecta) {
      /*alert("La Cédula ingresada es Incorrecta");*/
    }
    this.verificador = cedulaCorrecta;
  }
}
