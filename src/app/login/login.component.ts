import { Usuario } from './../models/usuario.model';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsuarioService } from '../services/service.index';

declare function init_plugins()
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  usu_email: string;
  recuerdame: boolean = false;

  constructor(public router: Router, public _usuarioService: UsuarioService) {}

  ngOnInit() {
    init_plugins();

    this.usu_email = localStorage.getItem('usu_email') || '';
    if (this.usu_email.length > 1) {
      this.recuerdame = true;
    }
  }

  ingresar(forma: NgForm) {
    if (forma.invalid) {
      return;
    }

    let usuario = new Usuario(
      null,
      null,
      null,
      forma.value.usu_email,
      forma.value.usu_password
    );

    this._usuarioService
      .login(usuario, forma.value.recuerdame)
      .subscribe(correcto => this.router.navigate(['/dashboard']));
    /* console.log(forma.valid);
    console.log(forma.value); */

    /* this.route.navigate(['/dashboard']); */
  }
}
