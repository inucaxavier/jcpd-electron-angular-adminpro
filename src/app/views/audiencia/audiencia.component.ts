import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Audiencia } from '../../models/audiencia';
import { Component, OnInit } from '@angular/core';
import { AudienciaService } from 'src/app/services/service.index';

@Component({
  selector: 'app-audiencia',
  templateUrl: './audiencia.component.html',
  styles: []
})
export class AudienciaComponent implements OnInit {
  audi: Audiencia[] = [];
  nForm: FormGroup;
  constructor(private audienciaS: AudienciaService, public router: Router) {}

  ngOnInit() {
    this.listarAudi();
    this.nForm = new FormGroup({
      caso_id: new FormControl(null),
      rau_fecha_conciliacion: new FormControl(null),
      rau_hora_audiencia: new FormControl(null),
      rau_observacion: new FormControl(null),
      rau_resumen: new FormControl(null),
      rau_conciliacion: new FormControl(null)
    });
  }

  guardarAudiencia(event: any) {
    let audiencia = new Audiencia(
      this.nForm.value.caso_id,
      this.nForm.value.rau_fecha_conciliacion,
      this.nForm.value.rau_hora_audiencia,
      this.nForm.value.rau_observacion,
      this.nForm.value.rau_resumen,
      this.nForm.value.rau_conciliacion
    );
    this.audienciaS.crearAudiencia(audiencia).subscribe(
      res => {
        console.log(audiencia);
      },
      err => {
        console.log(err);
      }
    );
  }
  listarAudi() {
    this.audienciaS.cargarAudiencia().subscribe((resp: any) => {
      this.audi = resp.audi;
      console.log(this.audi);
    });
  }
}
