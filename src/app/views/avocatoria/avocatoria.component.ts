import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Avocatoria } from '../../models/avocatoria';
import { AvocatoriaService } from 'src/app/services/service.index';

import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-avocatoria',
  templateUrl: './avocatoria.component.html',
  styles: []
})
export class AvocatoriaComponent implements OnInit {
  avo: Avocatoria[] = [];
  nForm: FormGroup;

  constructor(private avocatoriaS: AvocatoriaService, public router: Router) {}

  ngOnInit() {
    this.listarAvo();

    this.nForm = new FormGroup({
      anc_id: new FormControl(null),
      avo_fecha_conciliacion: new FormControl(null),
      avo_observaciones: new FormControl(null),
      avo_hora_audiencia: new FormControl(null)
    });
  }
  guardarAvocatoria(event: any) {
    let avocatoria = new Avocatoria(
      this.nForm.value.anc_id,
      this.nForm.value.avo_fecha_conciliacion,
      this.nForm.value.avo_observaciones,
      this.nForm.value.avo_hora_audiencia
    );
    this.avocatoriaS.crearAvocatoria(avocatoria).subscribe(
      res => {
        console.log(avocatoria);
      },
      err => {
        console.log(err);
      }
    );
  }

  listarAvo() {
    this.avocatoriaS.cargarAvocatoria().subscribe((resp: any) => {
      this.avo = resp.avocatorias;
      console.log(this.avo);
    });
  }
}
