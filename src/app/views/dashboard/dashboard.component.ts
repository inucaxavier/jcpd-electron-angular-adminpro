import { DatosDenunciante } from './../../models/datosDenunciante';
import { ChartType } from 'chart.js';
import { Component, OnInit } from '@angular/core';
import { Label } from 'ng2-charts';
import { DashboardService } from 'src/app/services/service.index';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: []
})
export class DashboardComponent implements OnInit {
  public pieChartLabels: Label[] = [
    ['Jordan'],
    ['San Luis'],
    ['Miguel Egas Cabezas Peguche'],
    ['Gonzales Suarez'],
    ['Pataqui'],
    ['San Jose de Quichinche'],
    ['San Juan de Ilumán'],
    ['San Pablo'],
    ['San Rafael'],
    ['Selva Alegre']
  ];
  public pieChartData: number[] = [
    300,
    500,
    100,
    30,
    20,
    10,
    40,
    400,
    430,
    500
  ];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColors = [
    {
      backgroundColor: [
        'rgba(255,0,0,0.3)',
        'rgba(0,255,0,0.3)',
        'rgba(0,255,0.3)',
        'rgba(0,200,0.3)',
        'rgba(0,25,0.3)',
        'rgba(60,0.3)',
        'rgba(30,300,0.3)',
        'rgba(0,300,255,0.3)',
        'rgba(0,0,335,0.3)',
        'rgba(50,50,25,0.3)'
      ]
    }
  ];
  parro1: DatosDenunciante[] = [];

  constructor(private parroSer: DashboardService) {}

  ngOnInit() {
    this.parroSer.refrescaNeeds.subscribe(() => {
      this.cargarParr1();
    });
    this.cargarParr1();
  }

  cargarParr1() {
    this.parroSer.cargarParro1().subscribe((resp: any) => {
      this.parro1 = resp.datosdenunciante;
      let a = this.parro1.length;
      console.log(a);
    });
  }
}
