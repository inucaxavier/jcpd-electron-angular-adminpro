import { AnalisisCasoComponent } from './analisis-caso/analisis-caso.component';
import { FiltroCasoPipe } from './../pipes/filtro-caso.pipe';
import { NgModule } from '@angular/core';
import { PAGES_ROUTES } from './pages.routes';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { CasosListComponent } from './casos-list/casos-list.component';
import { CasosFormComponent } from './casos-form/casos-form.component';

import { HttpClientModule } from '@angular/common/http';
import { Graficas1Component } from './graficas1/graficas1.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { CommonModule } from '@angular/common';

// Charts libreria ng2-charts
import { ChartsModule } from 'ng2-charts';
import { PdfMakeWrapper } from 'pdfmake-wrapper';
import pdfFonts from 'pdfmake/build/vfs_fonts'; // fonts provided for pdfmake

// Set the fonts to use
PdfMakeWrapper.setFonts(pdfFonts);

// Pipe para filtrar una busqueda
import { FilterPipe } from '../pipes/filter.pipe';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { UsuariosFilterPipe } from '../pipes/usuarios-filter.pipe';
import { RegisterComponent } from '../login/register.component';
import { DatosAfectadoComponent } from './datos-afectado/datos-afectado.component';
import { DatosDenunciadoComponent } from './datos-denunciado/datos-denunciado.component';
import { DatosDenuncianteComponent } from './datos-denunciante/datos-denunciante.component';
import { CasoComponent } from './caso/caso.component';
import { FormUploadComponent } from './upload/form-upload/form-upload.component';
import { ListUploadComponent } from './upload/list-upload/list-upload.component';
import { DetailsUploadComponent } from './upload/details-upload/details-upload.component';
import { AvocatoriaComponent } from './avocatoria/avocatoria.component';
import { AudienciaComponent } from './audiencia/audiencia.component';
import { ResolucionComponent } from './resolucion/resolucion.component';
import { EditarAfectadoComponent } from './datos-afectado/editar-afectado/editar-afectado.component';
import { HistoricosComponent } from './historicos/historicos.component';
import { AnalisisComponent } from './analisis-caso/analisis/analisis.component';

@NgModule({
  declarations: [
    /*     PagesComponent,
 */ DashboardComponent,
    ProgressComponent,
    Graficas1Component,
    CasosListComponent,
    FilterPipe,
    FiltroCasoPipe,
    AccountSettingsComponent,
    CasosFormComponent,
    UsuariosComponent,
    UsuariosFilterPipe,
    RegisterComponent,
    DatosAfectadoComponent,
    DatosDenunciadoComponent,
    DatosDenuncianteComponent,
    CasoComponent,
    FormUploadComponent,
    ListUploadComponent,
    DetailsUploadComponent,
    AvocatoriaComponent,
    AudienciaComponent,
    ResolucionComponent,
    EditarAfectadoComponent,
    AnalisisCasoComponent,
    HistoricosComponent,
    AnalisisComponent
    /* PaginatePipe */
  ],
  exports: [
    DashboardComponent,
    ProgressComponent,
    Graficas1Component,
    CasosListComponent
  ],
  imports: [
    SharedModule,
    FormsModule,
    HttpClientModule,
    CommonModule,
    ChartsModule,
    NgxPaginationModule,
    PAGES_ROUTES,
    ReactiveFormsModule,
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' })
  ]
})
export class PagesModule {}
