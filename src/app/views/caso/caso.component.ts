import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { JcpdService } from 'src/app/services/service.index';

@Component({
  selector: 'app-caso',
  templateUrl: './caso.component.html',
  styles: []
})
export class CasoComponent implements OnInit {
  caso: any = [];
  @Input() fileUpload: string;
  showFile = false;
  constructor(
    private route: ActivatedRoute,
    private casoService: JcpdService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      const caso_numcaso = params.caso_numcaso;
      this.casoService.getCaso(caso_numcaso).subscribe(caso => {
        this.caso = caso;
        console.log(caso);
      });
    });
  }
}

/*       this.caso = new Caso();*/
/* if (caso == undefined) {
  return;
}
this.caso = caso; */
