import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DatosCasoService } from 'src/app/services/service.index';
import { Router } from '@angular/router';
import { DatosDenunciante } from 'src/app/models/datosDenunciante';

@Component({
  selector: 'app-datos-denunciante',
  templateUrl: './datos-denunciante.component.html',
  styleUrls: []
})
export class DatosDenuncianteComponent implements OnInit {
  formaA: FormGroup;
  genero: any = [];
  identidad: any = [];
  caso: any = [];
  parroquia: any = [];
  discapacidad: any = [];
  tpersona: any = [];

  constructor(
    public afectadoService: DatosCasoService,
    public router: Router
  ) {}

  ngOnInit() {
    this.formaA = new FormGroup({
      caso_id: new FormControl(null),
      dden_nombre: new FormControl(null),
      dden_apellido: new FormControl(null),
      dden_ci: new FormControl(null),
      tper_id: new FormControl(null),
      dden_fecha_nacimento: new FormControl(null),
      dden_edad: new FormControl(null),
      dden_relacion_afectado: new FormControl(null),
      gene_id: new FormControl(null),
      iden_id: new FormControl(null),
      prov_id: new FormControl(null),
      can_id: new FormControl(null),
      sec_id: new FormControl(null),
      parr_id: new FormControl(null),
      dden_direccion: new FormControl(null),
      dden_telefono: new FormControl(null),
      dden_email: new FormControl(null),
      dden_observaciones: new FormControl(null)
    });

    this.afectadoService.getCaso().subscribe(res => {
      this.caso = res;
    });
    this.afectadoService.getGenero().subscribe(res => {
      this.genero = res;
    });
    this.afectadoService.getIdentidad().subscribe(res => {
      this.identidad = res;
    });
    this.afectadoService.getParroquia().subscribe(res => {
      this.parroquia = res;
    });
    this.afectadoService.getDiscapacidad().subscribe(res => {
      this.discapacidad = res;
    });
    this.afectadoService.getTpersona().subscribe(res => {
      this.tpersona = res;
    });
  }

  saveNewDenunciante() {
    /*  if (this.formaA.invalid) {
      console.log('error al ingresar datos');
      return;
    } */
    let denunciante = new DatosDenunciante(
      this.formaA.value.caso_id,
      this.formaA.value.dden_nombre,
      this.formaA.value.dden_apellido,
      this.formaA.value.dden_ci,
      this.formaA.value.tper_id,
      this.formaA.value.dden_fecha_nacimento,
      this.formaA.value.dden_edad,
      this.formaA.value.dden_relacion_afectado,
      this.formaA.value.gene_id,
      this.formaA.value.iden_id,
      this.formaA.value.prov_id,
      this.formaA.value.can_id,
      this.formaA.value.sec_id,
      this.formaA.value.parr_id,
      this.formaA.value.dden_direccion,
      this.formaA.value.dden_telefono,
      this.formaA.value.dden_email,
      this.formaA.value.dden_observaciones
    );
    this.afectadoService.crearDenunciante(denunciante).subscribe(
      res => {
        console.log('denunciante:', denunciante), this.router.navigate([
          '/casos-list'
        ]);
        this.formaA.reset();
      },
      err => {
        console.error(err), this.router.navigate(['/casos-list']);
      }
    );
  }
}
