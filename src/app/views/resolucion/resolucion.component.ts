import { Resolucion } from './../../models/resolucion';
import { Component, OnInit } from '@angular/core';
import { ResolucionService } from 'src/app/services/service.index';

@Component({
  selector: 'app-resolucion',
  templateUrl: './resolucion.component.html',
  styles: []
})
export class ResolucionComponent implements OnInit {
  listarRes: Resolucion[] = [];

  constructor(public resolucionSer: ResolucionService) {}

  ngOnInit() {
    this.cargarRes();
  }

  cargarRes() {
    this.resolucionSer.traerResolucion().subscribe((resp: any) => {
      this.listarRes = resp.resolucion;

      console.log(this.listarRes);
    });
  }
}
