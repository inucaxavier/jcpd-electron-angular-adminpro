import { DatosAfectado } from '../../models/datosAfectado';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DatosCasoService } from 'src/app/services/service.index';
import { Router } from '@angular/router';

@Component({
  selector: 'app-datos-afectado',
  templateUrl: './datos-afectado.component.html',
  styles: []
})
export class DatosAfectadoComponent implements OnInit {
  formaA: FormGroup;
  genero: any = [];
  identidad: any = [];
  caso: any = [];
  parroquia: any = [];
  discapacidad: any = [];

  constructor(
    public afectadoService: DatosCasoService,
    public router: Router
  ) {}

  ngOnInit() {
    this.formaA = new FormGroup({
      caso_id: new FormControl(null, Validators.required),
      daf_nombre: new FormControl(null, Validators.required),
      daf_apellido: new FormControl(null, Validators.required),
      gene_id: new FormControl(null, Validators.required),
      iden_id: new FormControl(null),
      dis_id: new FormControl(null),
      prov_id: new FormControl(null),
      can_id: new FormControl(null),
      parr_id: new FormControl(null, Validators.required),
      daf_ci: new FormControl(null),
      daf_fecha_nacimiento: new FormControl(null),
      daf_edad: new FormControl(null),
      daf_direccion: new FormControl(null, Validators.required),
      daf_telefono: new FormControl(null, Validators.required),
      daf_email: new FormControl(null, Validators.required),
      daf_obsevacion: new FormControl(null, Validators.required)
    });
    this.afectadoService.getCaso().subscribe(res => {
      this.caso = res;
    });
    this.afectadoService.getGenero().subscribe(res => {
      this.genero = res;
    });
    this.afectadoService.getIdentidad().subscribe(res => {
      this.identidad = res;
    });
    this.afectadoService.getParroquia().subscribe(res => {
      this.parroquia = res;
    });
    this.afectadoService.getDiscapacidad().subscribe(res => {
      this.discapacidad = res;
    });
  }

  saveNewAfectado() {
    if (this.formaA.invalid) {
      console.log('error al ingresar datos');
      return;
    }
    let afectado = new DatosAfectado(
      this.formaA.value.caso_id,
      this.formaA.value.daf_nombre,
      this.formaA.value.daf_apellido,
      this.formaA.value.daf_ci,
      this.formaA.value.daf_fecha_nacimiento,
      this.formaA.value.daf_edad,
      this.formaA.value.iden_id,
      this.formaA.value.gene_id,
      this.formaA.value.dis_id,
      this.formaA.value.prov_id,
      this.formaA.value.can_id,
      this.formaA.value.parr_id,
      this.formaA.value.daf_direccion,
      this.formaA.value.daf_telefono,
      this.formaA.value.daf_email,
      this.formaA.value.daf_obsevacion
    );
    this.afectadoService.crearAfectado(afectado).subscribe(
      res => {
        console.log('afectado', afectado), this.router.navigate([
          '/datosdenunciado'
        ]);
        this.formaA.reset();
      },
      err => {
        console.error(err), this.router.navigate(['/casos-list']);
      }
    );
  }
}
