import { Observable, Subject } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { CasosService, JcpdService } from '../../services/service.index';
import { Caso } from '../../models/caso.model';
declare var swal: any;

@Component({
  selector: 'app-historicos',
  templateUrl: './historicos.component.html',
  styleUrls: ['./historicos.component.css']
})
export class HistoricosComponent implements OnInit {
  casosli: Caso[] = [];
  casos: Caso[];
  cargando: boolean = true;
  filterCaso = '';
  filtroCaso = '';
  constructor(private casoSer: JcpdService) {}

  ngOnInit() {
    this.casoSer.refrescaNeeds.subscribe(() => {
      this.cargarCasos();
    });

    this.cargarCasos();
  }

  cargarCasos() {
    this.cargando = true;
    this.casoSer.cargarHistoricos().subscribe((resp: any) => {
      this.casosli = resp.casos;
      this.cargando = false;
    });
  }

  togglePublishState(caso: Caso): void {
    swal({
      title: 'Estas Seguro?',
      text: 'Esta a punto de activar el caso: ' + caso.caso_numcaso,
      icon: 'warning',
      buttons: true,
      dangerMode: true
    }).then(actualizar => {
      if (actualizar) {
        caso.published = !caso.published;
        this.casoSer.togglePublishState(caso).subscribe(
          result => {
            console.log(result);
            if (result) {
              const index: number = this.casos.findIndex(
                currentCaso => currentCaso.caso_id === result.caso_id
              );
              this.casos[index] = result;
              console.log(result);
              this.cargarCasos();
            }
          },
          error => {
            caso.published = !caso.published;
            console.error(error);
          }
        );
      }
    });
  }
}
