import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalisisCasoComponent } from './analisis-caso.component';

describe('AnalisisCasoComponent', () => {
  let component: AnalisisCasoComponent;
  let fixture: ComponentFixture<AnalisisCasoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalisisCasoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalisisCasoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
