import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JcpdService } from 'src/app/services/service.index';

@Component({
  selector: 'app-analisis',
  templateUrl: './analisis.component.html',
  styles: []
})
export class AnalisisComponent implements OnInit {
  caso: any = [];

  constructor(
    private route: ActivatedRoute,
    private casoService: JcpdService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      const caso_numcaso = params.caso_numcaso;
      this.casoService.getCaso(caso_numcaso).subscribe(caso => {
        this.caso = caso;
        console.log(caso);
      });
    });
  }
}
