import { Caso } from 'src/app/models/caso.model';
import { Component, OnInit } from '@angular/core';
import { CasosService } from 'src/app/services/service.index';

@Component({
  selector: 'app-analisis-caso',
  templateUrl: './analisis-caso.component.html',
  styleUrls: ['./analisis-caso.component.css']
})
export class AnalisisCasoComponent implements OnInit {
  casosli: Caso[] = [];
  filterCaso = '';
  filtroCaso = '';
  cargando: boolean = true;

  constructor(public casoServi: CasosService) {}

  ngOnInit() {
    this.cargarCasos();
  }

  cargarCasos() {
    this.cargando = true;

    this.casoServi.cargarCasos().subscribe((resp: any) => {
      this.casosli = resp.casos;
      this.cargando = false;
    });
  }
}
