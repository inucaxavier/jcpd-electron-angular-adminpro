import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DatosCasoService } from 'src/app/services/service.index';
import { Router } from '@angular/router';
import { DatosDenunciado } from 'src/app/models/datosDenunciado';

@Component({
  selector: 'app-datos-denunciado',
  templateUrl: './datos-denunciado.component.html',
  styles: []
})
export class DatosDenunciadoComponent implements OnInit {
  formaA: FormGroup;
  genero: any = [];
  identidad: any = [];
  caso: any = [];
  parroquia: any = [];
  discapacidad: any = [];
  tpersona: any = [];
  constructor(
    public afectadoService: DatosCasoService,
    public router: Router
  ) {}

  ngOnInit() {
    this.formaA = new FormGroup({
      caso_id: new FormControl(null, Validators.required),
      dag_nombre: new FormControl(null, Validators.required),
      dag_apellido: new FormControl(null, Validators.required),
      dag_ci: new FormControl(null),
      dag_relacion_afectado: new FormControl(null, Validators.required),
      tper_id: new FormControl(null),
      gene_id: new FormControl(null),
      iden_id: new FormControl(null),
      prov_id: new FormControl(null),
      can_id: new FormControl(null),
      sec_id: new FormControl(null),
      parr_id: new FormControl(null, Validators.required),
      dag_direccion: new FormControl(null, Validators.required),
      dag_telefono: new FormControl(null),
      dag_email: new FormControl(null),
      dag_observacion: new FormControl(null, Validators.required)
    });
    this.afectadoService.getCaso().subscribe(res => {
      this.caso = res;
    });
    this.afectadoService.getGenero().subscribe(res => {
      this.genero = res;
    });
    this.afectadoService.getIdentidad().subscribe(res => {
      this.identidad = res;
    });
    this.afectadoService.getParroquia().subscribe(res => {
      this.parroquia = res;
    });
    this.afectadoService.getDiscapacidad().subscribe(res => {
      this.discapacidad = res;
    });
    this.afectadoService.getTpersona().subscribe(res => {
      this.tpersona = res;
    });
  }
  saveNewDenunciado() {
    if (this.formaA.invalid) {
      console.log('error al ingresar datos');
      return;
    }
    let denunciado = new DatosDenunciado(
      this.formaA.value.caso_id,
      this.formaA.value.dag_nombre,
      this.formaA.value.dag_apellido,
      this.formaA.value.dag_ci,
      this.formaA.value.dag_relacion_afectado,
      this.formaA.value.tper_id,
      this.formaA.value.gene_id,
      this.formaA.value.iden_id,
      this.formaA.value.prov_id,
      this.formaA.value.can_id,
      this.formaA.value.sec_id,
      this.formaA.value.parr_id,
      this.formaA.value.dag_direccion,
      this.formaA.value.dag_telefono,
      this.formaA.value.dag_email,
      this.formaA.value.dag_observacion
    );
    this.afectadoService.crearDenunciado(denunciado).subscribe(
      res => {
        console.log('denunciado:', denunciado), this.router.navigate([
          '/datosdenunciante'
        ]);
        this.formaA.reset();
      },
      err => {
        console.error(err), this.router.navigate(['/casos-list']);
      }
    );
  }
}
