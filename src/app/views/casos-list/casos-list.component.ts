import { Observable, Subject } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { CasosService, JcpdService } from '../../services/service.index';
import { Caso } from '../../models/caso.model';
declare var swal: any;

@Component({
  selector: 'app-casos-list',
  templateUrl: './casos-list.component.html',
  styleUrls: ['./casos-list.component.css']
})
export class CasosListComponent implements OnInit {
  casosli: Caso[] = [];
  casos: Caso[];
  cargando: boolean = true;
  constructor(private casoSer: JcpdService) {}
  filterCaso = '';
  filtroCaso = '';

  ngOnInit() {
    this.casoSer.refrescaNeeds.subscribe(() => {
      this.cargarCasos();
    });

    this.cargarCasos();
  }

  cargarCasos() {
    this.cargando = true;
    this.casoSer.cargarCasos().subscribe((resp: any) => {
      this.casosli = resp.casos;
      this.cargando = false;
      console.log(this.casosli.length);
    });
  }

  togglePublishState(caso: Caso): void {
    swal({
      title: 'Estas Seguro?',
      text: 'Esta a punto de descativar el caso: ' + caso.caso_numcaso,
      icon: 'warning',
      buttons: true,
      dangerMode: true
    }).then(actualizar => {
      if (actualizar) {
        caso.published = !caso.published;
        this.casoSer.togglePublishState(caso).subscribe(
          result => {
            console.log(result);
            if (result) {
              const index: number = this.casos.findIndex(
                currentCaso => currentCaso.caso_id === result.caso_id
              );
              this.casos[index] = result;
              console.log(result);
              this.cargarCasos();
            }
          },
          error => {
            caso.published = !caso.published;
            console.error(error);
          }
        );
      }
    });
  }

  /* generatePDF() {
    const pdf = new PdfMakeWrapper();
    pdf.add(new Txt('hello world').bold().italics().end);
    pdf.create().download();
  }

  downloadPDF() {
    let div = document.getElementById('example1');
    const doc = new jsPDF();
    doc.fromHTML(div, 20, 30);

    doc.save('Test.pdf');
  } */
}
