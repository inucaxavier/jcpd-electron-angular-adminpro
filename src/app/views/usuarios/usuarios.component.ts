import { Usuario } from '../../models/usuario.model';
import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/service.index';

declare var swal: any;
@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styles: []
})
export class UsuariosComponent implements OnInit {
  usuarios: Usuario[] = [];
  filterPost = '';
  filtroPost = '';
  cargando: boolean = true;
  constructor(public _usuarioService: UsuarioService) {}

  ngOnInit() {
    this.cargarUsuarios();
  }
  // ====================================
  // Listar los usuarios
  // ====================================
  cargarUsuarios() {
    this.cargando = true;
    this._usuarioService.cargarUsuarios().subscribe((resp: any) => {
      this.usuarios = resp.usuarios;
      this.cargando = false;
    });
  }

  // ====================================
  // Borrar el usuario por completo
  // ====================================
  borrarUsuario(usuario: Usuario) {
    if (usuario.id === this._usuarioService.usuario.id) {
      swal(
        'No se puede borrar usuario',
        'No se puede borrar a si mismo',
        'error'
      );
      return;
    }

    swal({
      title: 'Estas Seguro?',
      text:
        'Esta apunto de borrar para siempre el usuario: ' + usuario.usu_nombres,
      icon: 'warning',
      buttons: true,
      dangerMode: true
    }).then(borrar => {
      console.log(borrar);
      if (borrar) {
        this._usuarioService.borrarUsuario(usuario.id).subscribe(borrado => {
          console.log(borrado);
          this.cargarUsuarios();
        });
      }
    });
  }

  // ====================================
  // Actualizar el rol del usuario
  // ====================================

  guardarUsuario(usuario: Usuario) {
    this._usuarioService.actualizarUsuarioRol(usuario).subscribe(resp => {
      console.log(resp);
    });
  }
}
