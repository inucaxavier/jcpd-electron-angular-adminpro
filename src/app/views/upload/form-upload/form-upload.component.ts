import { Component, OnInit, Input } from '@angular/core';
import { UploadFileService } from '../../../services/service.index';
import { HttpResponse, HttpEventType } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert';

@Component({
  selector: 'app-form-upload',
  templateUrl: './form-upload.component.html',
  styleUrls: ['./form-upload.component.css']
})
export class FormUploadComponent implements OnInit {
  selectedFiles: FileList;
  currentFileUpload: File;
  progress: { percentage: number } = { percentage: 0 };
  nForm: FormGroup;

  constructor(private uploadService: UploadFileService) {}
  @Input() archivoCaso: string;

  ngOnInit() {
    //this.nForm.reset();
    this.nForm = new FormGroup({
      caso_id: new FormControl(null, Validators.required)
    });
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  upload() {
    let casoId = this.nForm.value.caso_id;

    this.progress.percentage = 0;
    this.currentFileUpload = this.selectedFiles.item(0);
    this.uploadService
      .pushFileToStorage(this.currentFileUpload, casoId)
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress.percentage = Math.round(
            100 * event.loaded / event.total
          );
        } else if (event instanceof HttpResponse) {
          console.log('File is completely uploaded!');
          swal('El Archivo ha sido agregado a', casoId, 'success');
        }
      });

    this.selectedFiles = undefined;
  }
}
