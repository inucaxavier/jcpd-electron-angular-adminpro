import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UploadFileService } from 'src/app/services/service.index';

@Component({
  selector: 'app-list-upload',
  templateUrl: './list-upload.component.html',
  styles: []
})
export class ListUploadComponent implements OnInit {
  showFile = false;
  fileUploads: Observable<string[]>;
  constructor(private uploadService: UploadFileService) {}

  ngOnInit() {}

  showFiles(enable: boolean) {
    this.showFile = enable;

    if (enable) {
      this.fileUploads = this.uploadService.getFiles();
      console.log(this.fileUploads);
    }
  }
}
