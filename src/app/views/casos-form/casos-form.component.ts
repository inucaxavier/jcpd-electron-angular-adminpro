import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Caso } from '../../models/caso.model';
import { CasosService } from '../../services/service.index';
import { FormControl, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-casos-form',
  templateUrl: './casos-form.component.html',
  styles: []
})
export class CasosFormComponent implements OnInit {
  // edit: boolean = false;
  proco: any = [];
  tdenun: any = [];
  tda: any = [];
  ttda: any = [];
  nForm: FormGroup;

  constructor(private casosService: CasosService, public router: Router) {}

  ngOnInit() {
    this.nForm = new FormGroup({
      caso_id: new FormControl(null),
      caso_numcaso: new FormControl(null, Validators.required),
      proco_id: new FormControl(null, Validators.required),
      tda_id: new FormControl(null, Validators.required),
      caso_motivo: new FormControl(null, Validators.required),
      caso_observaciones: new FormControl(null, Validators.required),
      caso_fechareg: new FormControl(null, Validators.required),
      caso_fechaingreso: new FormControl(null, Validators.required),
      dav_id: new FormControl(null, Validators.required),
      tdenun_id: new FormControl(null, Validators.required),
      viewCount: new FormControl(0),
      published: new FormControl(true)
    });

    this.casosService.getProco().subscribe(res => {
      this.proco = res;
    });
    this.casosService.getTipoDenuncia().subscribe(res => {
      this.tdenun = res;
    });
    this.casosService.getTiposDerechosAmenazas().subscribe(res => {
      this.ttda = res;
    });
    this.casosService.getDerechoAmeViolentado().subscribe(res => {
      this.tda = res;
    });
  }

  saveNewCaso(event: any) {
    if (this.nForm.invalid) {
      return;
    }
    let caso = new Caso(
      this.nForm.value.caso_id,
      this.nForm.value.caso_numcaso,
      this.nForm.value.proco_id,
      this.nForm.value.tda_id,
      this.nForm.value.caso_motivo,
      this.nForm.value.caso_observaciones,
      this.nForm.value.caso_fechareg,
      this.nForm.value.caso_fechaingreso,
      this.nForm.value.dav_id,
      this.nForm.value.tdenun_id,
      this.nForm.value.viewCount,
      this.nForm.value.published
    );
    this.casosService.saveCaso(caso).subscribe(
      res => {
        console.log('caso', caso), this.router.navigate(['/datosafectado']);
        this.nForm.reset();
      },
      err => {
        console.error(err), this.router.navigate(['/casos-list']);
      }
    );
    event.target.disabled = true;
  }

  activarAfectado(event: any) {
    if (this.nForm.valid) {
      event.target.disabled = true;
    } else {
      event.target.disabled = false;
    }
  }
  mayus(e) {
    e.value = e.value.toUpperCase();
  }
}
