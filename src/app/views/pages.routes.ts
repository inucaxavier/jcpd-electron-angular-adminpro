import { AnalisisCasoComponent } from './analisis-caso/analisis-caso.component';
import { EditarAfectadoComponent } from './datos-afectado/editar-afectado/editar-afectado.component';
import { ResolucionComponent } from './resolucion/resolucion.component';
import { AudienciaComponent } from './audiencia/audiencia.component';
import { AvocatoriaComponent } from './avocatoria/avocatoria.component';
import { CasoComponent } from './caso/caso.component';
import { DatosDenuncianteComponent } from './datos-denunciante/datos-denunciante.component';
import { DatosDenunciadoComponent } from './datos-denunciado/datos-denunciado.component';
import { VerificaTokenGuard } from '../services/guards/verifica-token.guard';
import { AdminGuard } from '../services/service.index';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { CasosListComponent } from './casos-list/casos-list.component';
import { Graficas1Component } from './graficas1/graficas1.component';
import { ProgressComponent } from './progress/progress.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { CasosFormComponent } from './casos-form/casos-form.component';
import { RegisterComponent } from '../login/register.component';
import { DatosAfectadoComponent } from './datos-afectado/datos-afectado.component';
import { HistoricosComponent } from './historicos/historicos.component';
import { AnalisisComponent } from './analisis-caso/analisis/analisis.component';
const pagesRoutes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [VerificaTokenGuard],
    data: { titulo: 'Dashboard' }
  },
  {
    path: 'progress',
    component: ProgressComponent,
    data: { titulo: 'Progress' }
  },
  {
    path: 'graficas1',
    component: Graficas1Component,
    data: { titulo: 'Graficas' }
  },
  {
    path: 'casos-list',
    component: CasosListComponent,
    canActivate: [VerificaTokenGuard],
    data: { titulo: 'Casos' }
  },
  {
    path: 'casos-historicos',
    component: HistoricosComponent,
    canActivate: [VerificaTokenGuard],
    data: { titulo: 'Casos Historicos' }
  },
  {
    path: 'casos-form',
    component: CasosFormComponent,
    canActivate: [VerificaTokenGuard],
    data: { titulo: 'Registro de casos' }
  },
  {
    path: 'datosafectado',
    component: DatosAfectadoComponent,
    canActivate: [VerificaTokenGuard],
    data: { titulo: 'Registro de Datos Afectado' }
  },
  {
    path: 'datosdenunciado',
    component: DatosDenunciadoComponent,
    canActivate: [VerificaTokenGuard],
    data: { titulo: 'Registro de Datos Denunciado' }
  },
  {
    path: 'datosdenunciante',
    component: DatosDenuncianteComponent,
    canActivate: [VerificaTokenGuard],
    data: { titulo: 'Registro de Datos Denunciante' }
  },
  {
    path: 'avocatorias',
    component: AvocatoriaComponent,
    canActivate: [VerificaTokenGuard],
    data: { titulo: 'Registro de Datos Avocatoria' }
  },
  {
    path: 'resoluciones',
    component: ResolucionComponent,
    canActivate: [VerificaTokenGuard],
    data: { titulo: 'Registro de Datos Resolución' }
  },
  {
    path: 'audiencias',
    component: AudienciaComponent,
    canActivate: [VerificaTokenGuard],
    data: { titulo: 'Registro de Datos Audiencia' }
  },
  {
    path: 'analisis-caso',
    component: AnalisisCasoComponent,
    canActivate: [VerificaTokenGuard],
    data: { titulo: 'Registro de Datos Audiencia' }
  },
  {
    path: 'analisis-caso/:caso_numcaso',
    component: AnalisisComponent,
    canActivate: [VerificaTokenGuard],
    data: { titulo: 'Analisar CASO' }
  },
  {
    path: 'casos-list/:caso_numcaso',
    component: CasoComponent,
    canActivate: [VerificaTokenGuard],
    data: { titulo: 'Información Caso' }
  },
  {
    path: 'casos-list/:caso_numcaso/:daf_id',
    component: EditarAfectadoComponent,
    canActivate: [VerificaTokenGuard],
    data: { titulo: 'Información Caso' }
  },

  // Mantenimiento por parte del administrador
  {
    path: 'usuarios',
    component: UsuariosComponent,
    canActivate: [AdminGuard, VerificaTokenGuard],
    data: { titulo: 'Mantenimiento de usuarios' }
  },
  // Registro de Un Usuario Nuevo
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [AdminGuard, VerificaTokenGuard],
    data: { titulo: 'Registro de Usuario' }
  },

  { path: '', redirectTo: '/dashboard', pathMatch: 'full' }
];

export const PAGES_ROUTES = RouterModule.forChild(pagesRoutes);
