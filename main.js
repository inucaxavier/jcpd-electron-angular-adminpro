const {
  app,
  BrowserWindow
} = require('electron');

let win, serve;
const args = process.argv.slice(1);
serve = args.some(val => val === '--serve');

if (serve) {
  require('electron-reload')(__dirname, {});
}

function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 1100,
    height: 700,
    backgroundColor: '#ffffff',
    icon: `file://${__dirname}/dist/assets/images/jcpd/jcpd.png`
  });

  win.loadURL(`file://${__dirname}/dist/adminpro/index.html`);

  //// uncomment below to open the DevTools.
  // win.webContents.openDevTools()

  // Event when the window is closed.
  win.on('closed', function () {
    win = null;
  });
}

// Create window on electron intialization
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS specific close process
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function () {
  // macOS specific close process
  if (win === null) {
    createWindow();
  }
});